# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Rest.Repo.insert!(%Rest.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Rest.Repo
alias Rest.Directory.Game
Repo.insert! %Game{title: "Witcher3"}
Repo.insert! %Game{title: "Battlefield 1"}
Repo.insert! %Game{title: "Red Dead Redemption 2"}
